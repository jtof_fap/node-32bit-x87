# Node 32bit/x87 Docker image #
A container to run NodeJS on old architecture.

- Node version: `6.3.1`
- Npm version: `3.10.3`


### Purpose ###

I have created this docker image in order to build and run the latest `Node` version on an old 32bit server with athlon CPU (`i386/x87`: without SSE/SSE2 instructions) ...Fuck Yeah !!!...

Indeed, all other 32bit Node image doesn't work because they use the pre-builded [official Node binaries](https://nodejs.org/dist/latest/) compiled with standards x86 SSE/SSE2 instructions. When i run them => 'Illegal instruction' => Game over.

### Official image 32 bits tweaks ###

This image is derivated from the official node 6.3.1 image with some tweaks and with the last source code instead of use the pre-builded "Node" binaries:

- [https://github.com/nodejs/node](https://github.com/nodejs/node)
- [https://github.com/nodejs/node/blob/master/BUILDING.md](https://github.com/nodejs/node/blob/master/BUILDING.md)

With the following configure options:

    ./configure --without-snapshot --dest-cpu=ia32 --dest-os=linux

And some adjustments on the `common.gypi` file (thanks to [http://stackoverflow.com/a/36868753](http://stackoverflow.com/a/36868753) and [https://gcc.gnu.org/onlinedocs/gcc-6.1.0/gcc/x86-Options.html#x86-Options](https://gcc.gnu.org/onlinedocs/gcc-6.1.0/gcc/x86-Options.html#x86-Options)). I added the line:
	
	{
       'variables': {
		 ...
         'v8_target_arch': 'x87',
         ...

And replace:

    'conditions': [
          [ 'target_arch=="ia32"', {
            'cflags': [ '-m32' ],
            'ldflags': [ '-m32' ],

By:

    'conditions': [
          [ 'target_arch=="ia32"', {
            'cflags': [ '-m32', '-march=athlon', '-mfpmath=387' ],
            'ldflags': [ '-m32', '-march=athlon', '-mfpmath=387' ],

I lost some time to understand and get valid binaries, so for those that be interested or for other non standard platform, you could modified [this file](https://bitbucket.org/jtof_fap/node-32bit-x87/raw/7cd753c8c230654222425db5475e3746f914ea7d/x87/common.gypi) for your configuration.

### Getting this image ###

	docker pull insecurity/node-32bit-x87:latest

### Rebuild this image ###

	git clone git@bitbucket.org:jtof_fap/node-32bit-x87.git
	cd node-32bit-x87
	[modify x87/common.gypi or Dockerfile for your non standard architecture]
    docker build -t "node:latest" .
    [share...]

### How to use this image ###

This image is a base for node:onbuild image. You could use it to run a single Node.js script:

    docker run -it --rm --name NodeJS -v "$PWD":/srv/app -w /srv/app insecurity/node-32bit-x87:latest node your-daemon-or-script.js

For the rest, see the [node-32bit-x87-onbuild](https://hub.docker.com/r/insecurity/node-32bit-x87-onbuild) image:

### Documentation ###

See the official repository

- [https://hub.docker.com/_/node/](https://hub.docker.com/_/node/)

